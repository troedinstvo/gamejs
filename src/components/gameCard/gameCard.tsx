import React, {useEffect, useState} from 'react';

type GameCardProps = {
    activeCard: boolean,
    click: any,
    card: boolean,
    disabledClick: boolean,
    cards: boolean[]
}

export const GameCard: React.FC<GameCardProps> =({activeCard,disabledClick,card,click,cards})=> {
    const [activeItem, setActiveItem] = useState(activeCard);

    useEffect(() => {
        setActiveItem(activeCard);
    }, [activeCard]);


    return (
        <React.Fragment>
            <div className={activeItem ? "col active-item" : "col"}
                 onClick={() => {
                     if (card && disabledClick) {
                         setActiveItem(true);
                         setTimeout(() => setActiveItem(false), 965);
                     }
                     click(cards, card);
                 }}
            >
                <p style={{
                    borderRadius:card? 8: 10,
                }}>{card ? "YOU WIN" : null}</p>
            </div>
        </React.Fragment>

    );
};

export default GameCard;