import React, {FC} from 'react';
import {Redirect, RouteComponentProps} from "@reach/router";
import {checkAuthStatus} from "../../api/auth/auth";


const Authenticated: FC<RouteComponentProps> = ({children}) => {
    return checkAuthStatus() ? (
        <React.Fragment>
            {children}
        </React.Fragment>
    ) : (
        <Redirect to={"/"} noThrow={true} />
    );
}

export {Authenticated};