import React from 'react';

type RateProps = {
    chooseImage: any[]
}

export const Rate: React.FC<RateProps> = ({chooseImage})=> {
    return (
        <div className={"container rate-container"}>
            <h3>rate</h3>
            <div className="row ">
                {chooseImage.length > 0 ?
                    <React.Fragment>
                        {chooseImage.map((elem: any, key: number)=>
                            <div key={key}  className="gif-item">
                                <img src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150}/>
                            </div>
                        )}
                    </React.Fragment>
                    : null
                }
            </div>
        </div>
    );
};