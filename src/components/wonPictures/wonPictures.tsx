import React from 'react';

type WonPicturesProps = {
    gifCat: any[],
    chooseImage: any
}

export const WonPictures: React.FC<WonPicturesProps> = ({gifCat, chooseImage}) => {

    return (
        <div className="container won-container">
            <h3>won pictures</h3>
            <div className="wrap">
                {!!gifCat ? gifCat.map((elem: any, key: number) =>
                    <div className={"gif-item"} key={key} onClick={() => {

                        chooseImage([elem], key);
                    }}>
                        <img src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150}/>
                    </div>
                ) : <p>Firs rate free</p>}
            </div>
        </div>
    );
};

// export default WonPictures;