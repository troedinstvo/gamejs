import React from 'react';
import logo from '../../logo.svg';
import './App.scss';
import {Link, RouteComponentProps, Router} from '@reach/router'
import {GameBoard} from "../../pages/gameBoard";
import {UserBoard} from "../../pages/userBoard";
import {Login} from "../../pages/login";
import {SignUp} from "../../pages/signup";
import { Authenticated } from "../authComponent/authenticated";
import {logout, checkAuthStatus} from "../../api/auth/auth";


interface IAppProps extends RouteComponentProps {
    name: string;
}

const App: React.FC<IAppProps> = (props) => {
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    {props.name}
                </p>
            </header>
            <nav className={"navigation"}>
                {checkAuthStatus() ?
                    <React.Fragment>
                        <Link className={"item-nav"} to={"/game"}>Game</Link>
                        <Link className={"item-nav"} to={"/game-board"}>User Board</Link>
                        <button onClick={logout}>Выйти</button>
                    </React.Fragment>
                    :
                    <React.Fragment>
                        <Link className={"item-nav"} to={"/"}>Login</Link>
                        <Link className={"item-nav"} to={"/signup"}>Register</Link>
                    </React.Fragment>
                }
            </nav>
            {props.children}
        </div>
    );
};

const RoutedApp = () => {
    return (
        <Router>
            <App path={"/"} name="Thimbles">
                <Authenticated path={"/"}>
                    <GameBoard path={"/game"} default={true}/>
                    <UserBoard path={"/game-board"}/>
                </Authenticated>
                <Login path={"/"}/>
                <SignUp path={"/signup"}/>
            </App>
        </Router>
    )
}

export {RoutedApp}