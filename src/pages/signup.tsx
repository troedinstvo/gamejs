import React, {useState, FC} from 'react';
import { navigate, RouteComponentProps } from "@reach/router";
import { registration } from "../api/register/register";
import { User } from "../models/user";

const SignUp: FC<RouteComponentProps> = () => {
    const [user, setUser] = useState<User>({
        username: "",
        password: "",
        dataImage: []
    });

    const [notification, setNotification] = useState<string>('');

    const onInputChange = (fieldName: string) => (
        e: React.SyntheticEvent<HTMLInputElement>
    ): void => {
        setUser({
            ...user,
            [fieldName]: e.currentTarget.value,
        });
        setNotification('')
    };

    const onSubmit = (e: React.SyntheticEvent<HTMLFormElement>): void => {
        e.preventDefault();
        registration(user)
            .then(() => {
                navigate(`/game`)
            })
            .catch(err => {
                if (err.errorText) {
                    setNotification(err.errorText)
                } else {
                    // tslint:disable-next-line: no-console
                    console.warn('request problem', err)
                }
            })
    };

    return (
        <React.Fragment>
            <h2>Register</h2>
            <form onSubmit={onSubmit}>
                {notification ? <p>{notification}</p> : null}
                <input
                    type="text"
                    value={user.username}
                    onChange={onInputChange('username')}
                />
                <input
                    type="text"
                    value={user.password}
                    onChange={onInputChange('password')}
                />
                <button>register</button>
            </form>
        </React.Fragment>
    );
};

export { SignUp };