import React, {useState, FC} from 'react';
import { navigate, RouteComponentProps } from "@reach/router";
import { authentication } from "../api/auth/auth";
import { User } from "../models/user";

const Login: FC<RouteComponentProps> = () => {
    const [user, setUser] = useState<User>({
        username: "",
        password: "",
        dataImage: []
    });

    const [notification, setNotification] = useState<string>('');

    const onInputChange = (fieldName: string) => (
        e: React.SyntheticEvent<HTMLInputElement>
    ): void => {
        setUser({
            ...user,
            [fieldName]: e.currentTarget.value,
        });
        setNotification('')
    };

    const onSubmit = (e: React.SyntheticEvent<HTMLFormElement>): void => {
        e.preventDefault();
        authentication(user)
            .then(() => {
                navigate(`/game`)
            })
            .catch(err => {
                if (err.errorText) {
                    setNotification(err.errorText)
                } else {
                    // tslint:disable-next-line: no-console
                    console.warn('request problem', err)
                }
            })
    }

    return (
        <React.Fragment>
            <h2>Login</h2>
            <form onSubmit={onSubmit}>
                {notification ? <p>{notification}</p> : null}
                <input
                    type="text"
                    value={user.username}
                    onChange={onInputChange('username')}
                />
                <input
                    type="text"
                    value={user.password}
                    onChange={onInputChange('password')}
                />
                <button>Login</button>
            </form>
        </React.Fragment>
    );
};

export { Login };