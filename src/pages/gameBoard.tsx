import React, {useEffect, useState} from 'react';
import {RouteComponentProps} from '@reach/router'
import GameCard from "../components/gameCard/gameCard";
import {Rate} from "../components/rate/rate";
import {WonPictures} from "../components/wonPictures/wonPictures";
import "../assets/gameBoard.scss";
import {giphyURL} from "../api/image-api/imageApi";
import {addImage, userData} from "../api/userImage/userImage";
import axios from "axios";

const GameBoard: React.FC<RouteComponentProps> = (props) => {

    const [state, setState] = useState({
        chooseImage: [],
        userData: userData(),
        disabledClick: true,
        indexRemove: -1
    });
    const [shuffle, setShuffle] = useState({shuffleArr: [true, false, false]});
    const [activeCard, setActiveCard] = useState(false);

    function shuffleF(arr: boolean[]) {
        return arr.sort(() => Math.random() - 0.5);
    }

    const ImageItem = (chooseImage: any[]) => {
        // eslint-disable-next-line
        for (let key in chooseImage) {
            axios.get(giphyURL).then(function (response: any) {
                if (state.disabledClick) {
                    setState({
                        ...state,
                        userData: {...state.userData, userImage: state.userData.dataImage.push(response.data.data)}
                    });
                }
            }).finally(() => {
                if (chooseImage[key] !== 'new'){
                    setState({
                        ...state,
                        userData: {...state.userData,
                            userImage: state.userData.dataImage.push(chooseImage[key]),
                        },
                    })
                };
                addImage(state.userData.dataImage);
                setState({
                    ...state,
                    chooseImage: [],
                });
            });
        }
    };


    const clickElem = (el: any, currentElem: boolean) => {

        if (currentElem && state.disabledClick) {
            setActiveCard(false);
            if (state.chooseImage.length !== 0) {
                ImageItem(state.chooseImage);
            } else {
                ImageItem(["new"]);
            }

        } else if (state.disabledClick) {
            setActiveCard(true);
            setTimeout(() => setActiveCard(false), 965);
            setState({
                ...state,
                chooseImage: [],
            });
            addImage(state.userData.dataImage);
        }
        setTimeout(() => setShuffle({shuffleArr: shuffleF(el)}), 1100);
    };

    function chooseImage(element: any, key: number) {
        setState({
            ...state,
            chooseImage: state.chooseImage.concat(element),
            indexRemove: key,
            userData: {
                ...state.userData,
                userImage: state.userData.dataImage.splice(key, 1)
            }
        });
    }

    useEffect(() => {
        setShuffle({shuffleArr: shuffleF(shuffle.shuffleArr)});

    }, [shuffle.shuffleArr]);
    useEffect(()=>{

        if (state.userData.dataImage.length === 0) {
            setState({...state, disabledClick: true});
        } else if (state.userData.dataImage.length > 0 && state.chooseImage.length !== 0) {
            setState({...state, disabledClick: true});
        } else {
            setState({...state, disabledClick: false});
        }
        // eslint-disable-next-line
    }, [ state.userData, state.chooseImage]);
    // console.log(state);
    return (
        <React.Fragment>
            <h3>Game</h3>
            <div className="container game">
                {shuffle.shuffleArr.map((element: boolean, key: number) =>

                    <React.Fragment key={key}>
                        <GameCard activeCard={activeCard} click={clickElem} card={element}
                                  disabledClick={state.disabledClick} cards={shuffle.shuffleArr}/>
                    </React.Fragment>
                )}
            </div>
            <div className="gifs-container">
                <Rate chooseImage={state.chooseImage}/>
                <WonPictures gifCat={state.userData.dataImage} chooseImage={chooseImage}/>
            </div>
        </React.Fragment>
    );
};

export {GameBoard};