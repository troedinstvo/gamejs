import React, {useState} from 'react';
import { RouteComponentProps } from '@reach/router'
import {userData} from "../api/userImage/userImage";
import "../assets/user-board.scss"

const UserBoard: React.FC<RouteComponentProps>=()=> {

    const [state] = useState({
        userData: userData()
    });

    return (
        <React.Fragment>
            <div className={'container'}>
                <h3>User Board</h3>
                <div className="row">
                    <div className="wrap won-images">
                        {!!state.userData?state.userData.dataImage.map((elem: any, key:number) =>
                            <div className={"gif-item"} key={key}>
                                <img src={elem.images.downsized.url} alt={elem.title} width={"auto"} height={150} />
                            </div>
                        ):null}
                    </div>
                </div>
            </div>
        </React.Fragment>
    );
};

export {UserBoard};