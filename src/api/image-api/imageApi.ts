const giphy = {
    baseURL: "https://api.giphy.com/v1/gifs/",
    apiKey: "sXpGFDGZs0Dv1mmNFvYaGUvYwKX0PWIh",
    tag: "cats",
    type: "random",
    rating: "pg-13"
};

let giphyURL = encodeURI(
    giphy.baseURL +
    giphy.type +
    "?api_key=" +
    giphy.apiKey +
    "&tag=" +
    giphy.tag +
    "&rating=" +
    giphy.rating
);

export {giphyURL};