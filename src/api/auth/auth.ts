import { navigate } from "@reach/router";
import { User } from "../../models/user";

interface IAuthInterface {
    status: number;
    data: string;
    errorText: string;
}

const checkCredential = (data: User): boolean => {
    const user = JSON.parse(window.localStorage[data.username]);
    console.log(user);
    if (data.username === user.username && data.password === user.password) {
        return true
    } else {
        return false
    }
};

export const authentication = (data: User): Promise<IAuthInterface> => {
    const promise = new Promise<IAuthInterface> ((resolve, reject) => {
        if (!checkCredential(data)) {
            reject({
                status: 500,
                errorText: "incorrect login or password"
            })

        } else {
            window.localStorage.setItem('tstz.authenticated', 'true');
            window.localStorage.setItem('authenticated', data.username);
            // @ts-ignore
            resolve({
                status: 200,
                data: "ok"
            });
        }
    } );
    return promise
};
export const checkAuthStatus = (): boolean => {
    if (localStorage.getItem('tstz.authenticated')) {
        return true
    } else {
        return false
    }
}

// функция для логаута, принимает 0 аргументов
// ничего не возвращает (для этого используется тип void)

export const logout = (): void => {
    window.localStorage.removeItem('tstz.authenticated');
    navigate('/') // используем для переброса на новый url-адрес (reach-router)
}