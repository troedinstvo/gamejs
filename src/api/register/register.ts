import { navigate } from "@reach/router";
import { User } from "../../models/user";

interface IRegistrationInterface {
    status: number;
    data: string;
    errorText: string;
}

const checkUserCredential = (data: User): boolean => {
    const users = window.localStorage[data.username];
    if (!users) {
        window.localStorage[data.username] = JSON.stringify(data);
        return true
    } else {
        return false
    }
};

export const registration = (data: User): Promise<IRegistrationInterface> => {
    const promise = new Promise<IRegistrationInterface> ((resolve, reject) => {
        if (!checkUserCredential(data)) {
            reject({
                status: 500,
                errorText: "user exist"
            })

        } else {
            window.localStorage.setItem('tstz.authenticated', 'true');
            window.localStorage.setItem('authenticated', data.username);
            // @ts-ignore
            resolve({
                status: 200,
                data: "ok"
            });
        }
    } );
    return promise
};


export const logout = (): void => {
    window.localStorage.removeItem('tstz.authenticated');
    navigate('/')
};