const userData =  () => {
    const user = window.localStorage['authenticated'];
    const userDate = JSON.parse(window.localStorage[user]);
    return userDate
};

const addImage = (id: any) => {
    const withImage = userData();
    withImage["dataImage"] = id;
    window.localStorage[withImage.username] = JSON.stringify(withImage);
};

export {userData};
export {addImage};